# Alzheimer’s Dementia Recognition through Spontaneous Speech: the ADReSS Challenge

This repo contains information about the ADReSS Challenge, held at
Interspeech 2020, and the files for its website, now at Gitlab.

The website is at 
<https://luzs.gitlab.io/adress/>


## See also:

Luz S, Haider F, Fuente SDL, Fromm D, MacWhinney B. Alzheimer's Dementia Recognition through Spontaneous Speech: The ADReSS Challenge. In INTERSPEECH 2020. ISCA. 2020. p. 2172-2176 <https://doi.org/10.21437/Interspeech.2020-2571>


